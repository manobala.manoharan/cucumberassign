Feature: Validates Demo Web Shop Application

  Scenario Outline: Verify The Login Functionality in https://demowebshop.tricentis.com/
    Given Open the Website https://demowebshop.tricentis.com/
    When User Should Click On The Login button
    And User Should Enter The Email "<Email>" In Email Textbox
    And User Should Enter The Password "<Password>" In Password Textbox
    Then User Should Click On The Login button and It Should Navigates To HomePage

    Examples: 
      | Email                 | Password |
      | sadaaaa@gmail.com     | Sada@123 |
      | manuwarrier@gmail.com | Manu@123 |
