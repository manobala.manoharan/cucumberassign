package com.step;


import io.cucumber.testng.AbstractTestNGCucumberTests;
import io.cucumber.testng.CucumberOptions;

@CucumberOptions(features="src/test/resources/com/ref/refreshh.feature",glue="com.step")

public class RefreshRunner extends AbstractTestNGCucumberTests{

}
