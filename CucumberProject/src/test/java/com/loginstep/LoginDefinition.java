package com.loginstep;

import java.time.Duration;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

public class LoginDefinition {

	static WebDriver driver = null;
	
	@Given("Open the Website https:\\/\\/demowebshop.tricentis.com\\/")
	public void open_the_website_https_demowebshop_tricentis_com() {
		
		 driver = new ChromeDriver();
		
		driver.get("https://demowebshop.tricentis.com/");
		
		driver.manage().window().maximize();
		
		driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(10));
		
	}

	@When("User Should Click On The Login button")
	public void user_should_click_on_the_login_button() throws InterruptedException {
		
		driver.findElement(By.linkText("Log in")).click();
		
	}

	@When("User Should Enter The Email {string} In Email Textbox")
	public void user_should_enter_the_email_in_email_textbox(String string) {

		driver.findElement(By.id("Email")).sendKeys(string);
	}

	@When("User Should Enter The Password {string} In Password Textbox")
	public void user_should_enter_the_password_in_password_textbox(String string) {

		driver.findElement(By.id("Password")).sendKeys(string);
	}

	@Then("User Should Click On The Login button and It Should Navigates To HomePage")
	public void user_should_click_on_the_login_button_and_it_should_navigates_to_home_page() {
	
		driver.findElement(By.xpath("//input[@value='Log in']")).click();
	}

}
