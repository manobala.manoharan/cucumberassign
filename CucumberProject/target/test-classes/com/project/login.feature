Feature: Validates Demo Web Shop Application

  Scenario: Verify The Login Functionality in https://demowebshop.tricentis.com/
    Given Open the Website https://demowebshop.tricentis.com/
    When User Should Click On The Login button
    And User Should Enter The Email "manuwarrier@gmail.com" In Email Textbox
    And User Should Enter The Password "Manu@123" In Password Textbox
    Then User Should Click On The Login button and It Should Navigates To HomePage
